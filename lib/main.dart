import 'package:flutter/material.dart';
import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:pretty_qr_code/pretty_qr_code.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'dart:core';

// import './scanner.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<BluetoothDevice> devices = [];
  BluetoothDevice? selectedDevice;
  BlueThermalPrinter printer = BlueThermalPrinter.instance;
  // Barcode? _result;
  // QRViewController? controller;
  // final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  late TextEditingController _outputController;
  late String _result="";

  @override
  void initState() {
    super.initState();
    getDevices();
    _outputController = TextEditingController();
  }

  void getDevices() async {
    devices = await printer.getBondedDevices();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Core Sampler"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DropdownButton<BluetoothDevice>(
                value: selectedDevice,
                hint: const Text('Select print'),
                onChanged: (device) {
                  setState(() {
                    selectedDevice = device;
                  });
                },
                items: devices
                    .map((e) => DropdownMenuItem(
                          value: e,
                          child: Text(e.name!),
                        ))
                    .toList()),
            const SizedBox(
              height: 10,
            ),
            // _prett(),
            //   SizedBox(
            //   height: 400,
            //   child: PrettyQr(
            //     image: const AssetImage('assets/images/logo.png'),
            //     size: 300,
            //     data: _outputController.text,
            //     errorCorrectLevel: QrErrorCorrectLevel.M,
            //     typeNumber: null,
            //     roundEdges: true,
            //   ),
            // ),
            const SizedBox(
              height: 10,
            
            ),
              Text(_outputController.text),
            const SizedBox(
              height: 20,
            ),
            const SizedBox(height: 10),
            TextField(
              controller: _outputController,
              maxLines: 1,
              decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.book_online_outlined),
                  helperText: 'Barcode',
                  hintText: 'Barcode hasil Scan',
                  hintStyle: TextStyle(fontSize: 15),
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 5, vertical: 10)),
            ),
            
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.center,

              children: [
                const SizedBox(
                  width: 10,
                ),
                ElevatedButton.icon(
                    onPressed: () {
                      printer.connect(selectedDevice!);
                    },
                    icon: const Icon(Icons.bluetooth_connected_outlined, size: 30),
                    label: const Text("connect")),

                // const Divider(
                //   height: 30,
                // ),
                const SizedBox(width: 10),
                ElevatedButton.icon(
                    onPressed: () {
                      printer.disconnect();
                    },
                    icon: const Icon(Icons.network_check, size: 30),
                    label: const Text("Disconnect")),
                const SizedBox(
                  width: 10,
                ),
                ElevatedButton.icon(
                    onPressed: () async {
                      if ((await printer.isConnected)!) printer.printNewLine();
                      // SIZE
                      // 0: Normal
                      // 1: Normal - Bold
                      // 2: Medium - Bold
                      // 3: Large - Bold

                      //ALIGN
                      // 0: left
                      // 1: center
                      // 2: right
                      // printer.
                      printer.printCustom('Core Sampler ', 2, 1);
                      printer.printLeftRight("-----", "-----", 3);
                      printer.printCustom(_outputController.text, 3, 1);
                      printer.printLeftRight("-------", "-------", 3);
                      printer.printQRcode(_outputController.text, 250, 250, 1);
                      printer.printNewLine();
                      printer.printNewLine();
                      printer.printNewLine();
                      printer.printNewLine();
                      printer.printNewLine();
                      printer.paperCut();
                    },
                    icon: const Icon(Icons.print_outlined,size: 30),
                    label: const Text("Print")
                    ),

                // const SizedBox(
                //   height: 100.0,
                //   width: 100.0,
                // ),
              const SizedBox(
                width: 10,
              ),
              ],
              
            ),
            const SizedBox(height: 20),
            // // Row(hei),
            // ElevatedButton(
            //     onPressed: () {
            //       printer.connect(selectedDevice!);
            //     },
            //     child: const Text("connect")),
            // ElevatedButton(
            //     onPressed: () {
            //       printer.disconnect();
            //     },
            //     child: const Text("diskonek")),
            // ElevatedButton(
            //     onPressed: () async {
            //       if ((await printer.isConnected)!) printer.printNewLine();
            //       // SIZE
            //       // 0: Normal
            //       // 1: Normal - Bold
            //       // 2: Medium - Bold
            //       // 3: Large - Bold

            //       //ALIGN
            //       // 0: left
            //       // 1: center
            //       // 2: right
            //       // printer.
            //       printer.printCustom('Core Sampler ', 2, 1);
            //       printer.printLeftRight("-----", "-----", 3);
            //       printer.printCustom(_outputController.text, 3, 1);
            //       // printer.printCustom("123456789012345678912345678", 3, 1);//Max 25 char
            //       printer.printLeftRight("-------", "-------", 3);
            //       printer.printQRcode(_outputController.text, 250, 250, 1);
            //       // printer.printQRcode(textToQR, width, height, align)
            //       printer.printNewLine();
            //       printer.printNewLine();
            //       // printer.print3Column("coba", "string2", "string3", 1);
            //       // printer.print3Column("coba", "string2", "string3", 1);
            //       // printer.print3Column("coba", "string2", "string3", 1);

            //       // printer.printCustom(_outputController.text, 0, 1);
            //       // printer.printQRcode(_outputController.text, 200, 200, 1);

            //       // printer.printNewLine();
            //       printer.printNewLine();
            //       printer.printNewLine();
            //       printer.printNewLine();
            //       // printer.printCustom("Thank You", 3, 1);
            //       printer.paperCut();
            //     },
            //     child: const Text("Print")
            //     ),
            ElevatedButton.icon(
              onPressed: () => _scan(),
              icon: const Icon(Icons.qr_code_scanner, size: 100),
              label: const Text("SCAN"),
            ),
          ],
        ),
      ),
    );
  }

  Future _scan() async {
    await Permission.camera.request();
    String? barcode = await scanner.scan();
    if (barcode == null) {
      print('nothing return.');
    } else {
      _outputController.text = barcode;
    }
  }
  // Widget _prett() {
  //   return SizedBox(
  //     child: PrettyQr(
  //               image: const AssetImage('assets/images/logo.png'),
  //               size: 300,
  //               data: _outputController.text,
  //               errorCorrectLevel: QrErrorCorrectLevel.M,
  //               typeNumber: null,
  //               roundEdges: true,
  //             ),
  //   );
  // }
}
